package com.sapient.fooddeleveriapp.dao;

import java.util.List;

import com.sapient.fooddeleveriapp.exception.SubscriptionNotFoundException;
import com.sapient.fooddeleveriapp.model.Subscription;

/**
 * @author subdas3
 *
 */
public interface SubscriptionDAO {
	List<Subscription> getAllsubscriptions();
	Subscription getSubscriptionById(int subscriptionId)throws SubscriptionNotFoundException ;
	Subscription insertSubscription(Subscription subscription);
	boolean deleteSubscriptionById(int subscriptionId)throws SubscriptionNotFoundException;
	//Subscription updateSubscription(Subscription subscription)throws SubscriptionNotFoundException;
	
	

}
