package com.sapient.fooddeleveriapp.dao;

import java.util.List;

import com.sapient.fooddeleveriapp.exception.FoodNotFoundException;
import com.sapient.fooddeleveriapp.model.Food;

public interface FoodDAO {
	
	void addFood(Food food);
	boolean deleteFood(int foodId); 
	Food getFoodByCategoryId(int categoryId) ; 
	Food getFoodByid(int foodid) ; 
	boolean updateFood(int foodid, int price);
	
	List<Food> getAllFoods();
	List<Food> getAllFoodsByCategoryId(int categoryId) ;
	//List<Food> getFoodByCategory(String category)throws FoodNotFoundException;
	


   
	
}



	
	
	
	
	



