package com.sapient.fooddeleveriapp.dao;

import java.util.List;

import com.sapient.fooddeleveriapp.exception.OrderNotFoundExeception;
import com.sapient.fooddeleveriapp.exception.OrderTableNotFoundException;
import com.sapient.fooddeleveriapp.model.OrderTable;

public interface OrderTableDAO {
	
	public OrderTable addOrderTable(OrderTable orderTable);
	public boolean deleteOrderTableById(int orderid);
	public boolean updateOrderTableById(double total,int orderid);
	List<OrderTable> getAllOrderTables();
	List<OrderTable> getOrderTableByOrderId(int orderId) throws OrderTableNotFoundException;
	List<OrderTable> getAllOrderTablesBySubscriptionId(int subscriptionId) throws OrderTableNotFoundException;
	OrderTable getOrderByCustomerId(int customerId) throws OrderTableNotFoundException;
	
	List<OrderTable> getAllOrderTablesByStatus(String orderStatus) throws OrderTableNotFoundException;
}
