package com.sapient.fooddeleveriapp.dao;

import java.util.List;

import com.sapient.fooddeleveriapp.exception.CategoryNotFoundException;
import com.sapient.fooddeleveriapp.model.Category;
import com.sapient.fooddeleveriapp.model.Food;

public interface CategoryDAO {

	void addCategory(Category category);
	boolean deleteCategory(int categoryId);
	Category getCategoryById(int categoryId) throws CategoryNotFoundException;
	boolean updateCategory(int categoryId);
	
	List<Category> getAllCategories();
	
}
