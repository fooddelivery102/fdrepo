package com.sapient.fooddeleveriapp.dao;

import com.sapient.fooddeleveriapp.model.Customer;

public interface CustomerDAO {
	
	boolean addCustomer(Customer customer);
	boolean deleteCustomer(int CustomerId); 
	Customer getCustomerById(int CustomerId); 
	boolean updateCustomer(int CustomerId);

	
	Customer authenticateCustomer(String CustomerPhonenbr,String Customerpassword);
}
