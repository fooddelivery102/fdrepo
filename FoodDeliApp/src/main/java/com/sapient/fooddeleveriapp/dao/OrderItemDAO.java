package com.sapient.fooddeleveriapp.dao;

import java.util.List;

import com.sapient.fooddeleveriapp.exception.FoodNotFoundException;
import com.sapient.fooddeleveriapp.exception.OrderItemNotFoundException;
import com.sapient.fooddeleveriapp.model.OrderItem;

public interface OrderItemDAO {

	public OrderItem addOrderItem(OrderItem orderItem);
	public boolean deleteOrderItemById(int orderitemId);
	public boolean updateOrderItemById(int orderitemId,int quantity);
	List<OrderItem> getAllOrderItems();
	List<OrderItem> getOrderItemByOrderId(int orderId);// throws OrderItemNotFoundException;
	//edited by Subha
	OrderItem getOrderItemById(int orderitemId) ;//throws OrderItemNotFoundException;
}
