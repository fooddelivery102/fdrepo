package com.sapient.fooddeleveriapp.dao;

import java.util.List;

import com.sapient.fooddeleveriapp.exception.SubscriptionTypeNotFoundException;
import com.sapient.fooddeleveriapp.model.Subscription;
import com.sapient.fooddeleveriapp.model.SubscriptionType;

/**
 * @author Subhadeep Das
 *
 */
public interface SubscriptionTypeDAO {
	
	List<SubscriptionType> getAllSubscriptionTypes();
	SubscriptionType getSubscriptionTypeById(int subscriptionId) throws SubscriptionTypeNotFoundException;
	
	

}
