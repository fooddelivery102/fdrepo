package com.sapient.fooddeleveriapp.model;

public class Customer {
	
	private int customerId;
	private String customerName;
	private  String customerPhonenbr;
    private String customerAddress;
    private String password;
    
    public Customer() {}
    
	public Customer(String customerName, String customerPhonenbr, String customerAddress,
			String password) {
		super();
	//	this.customerId = customerId;
		this.customerName = customerName;
		this.customerPhonenbr = customerPhonenbr;
		this.customerAddress = customerAddress;
		this.password = password;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPhonenbr() {
		return customerPhonenbr;
	}
	public void setCustomerPhonenbr(String customerPhonenbr) {
		this.customerPhonenbr = customerPhonenbr;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	/*....*/
    
    
}
