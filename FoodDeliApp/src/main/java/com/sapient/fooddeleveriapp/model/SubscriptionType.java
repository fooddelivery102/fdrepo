package com.sapient.fooddeleveriapp.model;



/**
 * @author Subhadeep Das
 *
 */
public class SubscriptionType {
	private int subscriptionTypeId;
	private String subscriptionType;
	public int getSubscriptionTypeId() {
		return subscriptionTypeId;
	}
	public void setSubscriptionTypeId(int subscriptionTypeId) {
		this.subscriptionTypeId = subscriptionTypeId;
	}
	public String getSubscriptionType() {
		return subscriptionType;
	}
	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	} 
	
	
	
	
}
