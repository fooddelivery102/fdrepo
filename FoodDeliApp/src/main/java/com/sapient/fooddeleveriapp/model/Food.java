package com.sapient.fooddeleveriapp.model;

public class Food {
	
	private int foodId ;
	private String foodName ;
	private int categoryId;
	private String description;
	private int price;
	
	
	/*public Food(int foodId, String foodName, int categoryId, String description, int price) {
		super();
		this.foodId = foodId;
		this.foodName = foodName;
		this.categoryId = categoryId;
		this.description = description;
		this.price = price;
	}*/
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getFoodId() {
		return foodId;
	}
	public void setFoodId(int foodId) {
		this.foodId = foodId;
	}
	public String getFoodName() {
		return foodName;
	}
	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "Food [foodId=" + foodId + ", foodName=" + foodName + ", categoryId=" + categoryId + ", description="
				+ description + ", price=" + price + "]";
	}
	
	
}
	
	