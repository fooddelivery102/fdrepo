package com.sapient.fooddeleveriapp.model;



import java.sql.Date;
import java.time.LocalDateTime;

/**
 * @author Subhadeep Das
 *
 */
public class Subscription {
	private int subscriptionId;
	private Date subscriptionDate;
	private int subscriptionTime;
	private int customerId;
	private int subscriptionTypeId;
	public int getSubscriptionId() {
		return subscriptionId;
	}
	public void setSubscriptionId(int subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	
	public Date getSubscriptionDate() {
		return subscriptionDate;
	}
	public void setSubscriptionDate(Date subscriptionDate) {
		this.subscriptionDate = subscriptionDate;
	}
	public int getSubscriptionTime() {
		return subscriptionTime;
	}
	public void setSubscriptionTime(int subscriptionTime) {
		this.subscriptionTime = subscriptionTime;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getSubscriptionTypeId() {
		return subscriptionTypeId;
	}
	public void setSubscriptionTypeId(int subscriptionTypeId) {
		this.subscriptionTypeId = subscriptionTypeId;
	}
	
	
	
	

}
