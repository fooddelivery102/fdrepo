package com.sapient.fooddeleveriapp.model;

public class OrderItem {

	private int foodId;
	private int orderId;
	private int quantity;
	private int subtotal;
	private int orderitemId;
	
	
	
	/*public OrderItem(int foodId, int orderId, int quantity, int subtotal, int orderitemId) {
		super();
		this.foodId = foodId;
		this.orderId = orderId;
		this.quantity = quantity;
		this.subtotal = subtotal;
		this.orderitemId = orderitemId;
	}*/
	public int getFoodId() {
		return foodId;
	}
	public void setFoodId(int foodId) {
		this.foodId = foodId;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public int getOrderitemId() {
		return orderitemId;
	}
	public void setOrderitemId(int orderitemId) {
		this.orderitemId = orderitemId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(int subtotal) {
		this.subtotal = subtotal;
	}
	@Override
	public String toString() {
		return "OrderItem [foodId=" + foodId + ", orderitemId=" + orderitemId + ", quantity=" + quantity + ", subtotal="
				+ subtotal + "]";
	}
	
	
	
}
