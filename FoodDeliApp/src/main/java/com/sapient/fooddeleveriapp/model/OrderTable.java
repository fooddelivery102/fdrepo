package com.sapient.fooddeleveriapp.model;

import java.sql.Date;

public class OrderTable {
	
	private int orderId;
	private int foodId; 
	private String billingAddress ;
	private String orderStatus;
	private int customerId;
	private int subcriptionId;
	private Date orderDate;
	private Double total;
	
	
	
	
	public Double getTotal() {
		return total;
	}


	public void setTotal(Double total) {
		this.total = total;
	}


	public OrderTable() {
		super();
	}


	/*public OrderTable(int orderId, int foodId, String billingAddress, String orderStatus, int customerId,
			int subcriptionId, Date orderDate) {
		super();
		this.orderId = orderId;
		this.foodId = foodId;
		this.billingAddress = billingAddress;
		this.orderStatus = orderStatus;
		this.customerId = customerId;
		this.subcriptionId = subcriptionId;
		this.orderDate = orderDate;
	}*/
	
	
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public int getFoodId() {
		return foodId;
	}
	public void setFoodId(int foodId) {
		this.foodId = foodId;
	}
	
	public String getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getSubcriptionId() {
		return subcriptionId;
	}
	public void setSubcriptionId(int subcriptionId) {
		this.subcriptionId = subcriptionId;
	}
	@Override
	public String toString() {
		return "Order_table [orderId=" + orderId + ", foodId=" + foodId + ", billingAddress=" + billingAddress
				+ ", orderStatus=" + orderStatus + ", customerId=" + customerId + ", subcriptionId=" + subcriptionId
				+ ", orderDate=" + orderDate + "]";
	}
	
	
	
	
	
	

}
