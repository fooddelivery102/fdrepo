package com.sapient.fooddeleveriapp.exception;

public class FoodNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public FoodNotFoundException(String msg) {
		super(msg);
	}

}
