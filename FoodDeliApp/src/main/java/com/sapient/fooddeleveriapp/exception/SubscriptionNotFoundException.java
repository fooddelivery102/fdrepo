package com.sapient.fooddeleveriapp.exception;

public class SubscriptionNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SubscriptionNotFoundException(String msg) {
		super(msg);
	}

}
