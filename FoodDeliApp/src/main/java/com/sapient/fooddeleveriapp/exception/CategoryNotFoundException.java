package com.sapient.fooddeleveriapp.exception;

public class CategoryNotFoundException extends Exception {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public CategoryNotFoundException() 
	{}
	public CategoryNotFoundException(String s) 
	{
		super(s);
	}
}
