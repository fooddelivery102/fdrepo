package com.sapient.fooddeleveriapp.exception;

public class OrderTableNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public  OrderTableNotFoundException(String msg)
	{
		super(msg);
	}
	
}
