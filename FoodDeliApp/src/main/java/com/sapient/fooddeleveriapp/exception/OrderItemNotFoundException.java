package com.sapient.fooddeleveriapp.exception;

public class OrderItemNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OrderItemNotFoundException(String msg) {
		super(msg);
	}

	
	
}
