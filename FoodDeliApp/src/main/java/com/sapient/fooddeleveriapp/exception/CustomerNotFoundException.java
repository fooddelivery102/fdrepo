package com.sapient.fooddeleveriapp.exception;

public class CustomerNotFoundException extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerNotFoundException() {}
	
	public CustomerNotFoundException(String s) {
		super(s);
	}

}

