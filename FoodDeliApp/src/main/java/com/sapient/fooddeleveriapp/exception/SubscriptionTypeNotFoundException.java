package com.sapient.fooddeleveriapp.exception;

/**
 * @author Subhadeep Das
 *
 */
public class SubscriptionTypeNotFoundException extends Exception {
	
	public SubscriptionTypeNotFoundException(String m){
		super(m);
	}
}
