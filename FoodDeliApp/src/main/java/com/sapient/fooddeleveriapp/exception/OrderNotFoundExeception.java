package com.sapient.fooddeleveriapp.exception;

public class OrderNotFoundExeception extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public OrderNotFoundExeception(String msg) {
		super(msg);
	}

}
