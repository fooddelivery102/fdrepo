package com.sapient.fooddeleveriapp.dbconfig;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


	public class DBConnection {
		
		static Connection connection;
		/*....*/
		public static Connection openConnection() {
			Properties properties =new Properties();
			try {
				properties.load(new FileReader("src/main/resources/jdbc.properties"));
				
			}catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}catch (IOException e) {
				System.out.println(e.getMessage());
			}
			String drivername=(String) properties.get("oracle.driver");
			String url=(String) properties.get("oracle.url");
			String username=(String) properties.get("oracle.username");
			String password=(String) properties.get("oracle.password");
			connection=null;
			try {
				Class.forName(drivername);
				connection=DriverManager.getConnection(url, username, password);
			} catch (ClassNotFoundException | SQLException e) {
				System.out.println(e.getMessage());
			}
			return connection;
		}
		
		public static void closeConnection() {
			try {
				if(connection!=null)
					connection.close();
	} catch (SQLException e) {
		System.out.println(e.getMessage());
			}
		}

	}
