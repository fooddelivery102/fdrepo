package com.sapient.fooddeleveriapp.client;

import java.util.List;
import java.util.Scanner;

import com.sapient.fooddeleveriapp.dao.CustomerDAO;
import com.sapient.fooddeleveriapp.dao.FoodDAO;
import com.sapient.fooddeleveriapp.dao.OrderItemDAO;
import com.sapient.fooddeleveriapp.daoimpl.CustomerDAOImpl;
import com.sapient.fooddeleveriapp.daoimpl.FoodDAOImpl;
import com.sapient.fooddeleveriapp.daoimpl.OrderItemDaoImpl;
import com.sapient.fooddeleveriapp.model.Customer;
import com.sapient.fooddeleveriapp.model.Food;
import com.sapient.fooddeleveriapp.model.OrderItem;
import com.sapient.fooddeleveriapp.model.OrderTable;
import com.sapient.fooddeleveriapp.service.OrderService;
import com.sapient.fooddeleveriapp.serviceimpl.OrderServiceImpl;

public class ManagerUI {

	public static void main(String[] args) {
		OrderService orderService=new OrderServiceImpl();
		OrderItemDAO orderItemDAO=new OrderItemDaoImpl();
		FoodDAO foodDAO=new FoodDAOImpl();
		CustomerDAO customerDAO=new CustomerDAOImpl();
		// TODO Auto-generated method stub
		boolean looper=true;
		while(looper) {
			Scanner sc=new Scanner(System.in);
			List<OrderTable> orderTables=orderService.getAllNewOrders();
			System.out.println("Enter 1 for Orderlist, Enter 2 to exit");
			if(sc.nextInt()==1) {
				for(OrderTable orderTable:orderTables) {
					System.out.println("Order Id:"+orderTable.getOrderId());
					Customer customer=customerDAO.getCustomerById(orderTable.getCustomerId());
					System.out.println("Customer Name: "+customer.getCustomerName());
					System.out.println("Order Address:"+orderTable.getBillingAddress());
					System.out.println("***Item***");
					for(OrderItem orderItem:orderItemDAO.getOrderItemByOrderId(orderTable.getOrderId())) {
						
						Food food=foodDAO.getFoodByid(orderItem.getFoodId());
						System.out.println("Food :"+food.getFoodName()+" quantity: "+orderItem.getQuantity());
						
						
					}
					System.out.println("Total Ammount:"+orderTable.getTotal());
				}
				
			}else {
				
				looper=false;
				System.out.println("**Good bye***");
			}
		}

	}

}
