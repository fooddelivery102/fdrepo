package com.sapient.fooddeleveriapp.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.sapient.fooddeleveriapp.daoimpl.FoodDAOImpl;
import com.sapient.fooddeleveriapp.exception.FoodNotFoundException;
import com.sapient.fooddeleveriapp.model.*;
import com.sapient.fooddeleveriapp.service.CategoryService;
import com.sapient.fooddeleveriapp.service.CustomerService;
import com.sapient.fooddeleveriapp.service.FoodService;
import com.sapient.fooddeleveriapp.service.OrderService;
import com.sapient.fooddeleveriapp.serviceimpl.CategoryServcieImpl;
import com.sapient.fooddeleveriapp.serviceimpl.CustomerServiceImpl;
import com.sapient.fooddeleveriapp.serviceimpl.FoodServiceImpl;
import com.sapient.fooddeleveriapp.serviceimpl.OrderItemServiceImpl;
import com.sapient.fooddeleveriapp.serviceimpl.OrderServiceImpl;
import com.sun.corba.se.impl.resolver.ORBDefaultInitRefResolverImpl;

/**
 * @author subdas3
 *
 */
public class CustomerUI {

	public static void main(String[] args) {
		
		
		
		CustomerService customerService=new CustomerServiceImpl();
		CategoryService categoryService=new CategoryServcieImpl();
		FoodService foodService=new FoodServiceImpl();
		OrderService orderService=new OrderServiceImpl();
		Scanner sc=new Scanner(System.in);
		boolean looper=true;
		while(looper) {
			System.out.println("Welcome to the food delivery app. To signup enter 1. To Login enter2 To Exit enter 3" );
			switch (sc.nextInt()) {
				case 1:{
					System.out.println("Please provide the information");
					Customer customer=new Customer();
					System.out.println("Enter your Name:");
					customer.setCustomerName(sc.next());
					System.out.println("Enter your Adrress:");
					customer.setCustomerAddress(sc.next());
					System.out.println("Enter your Phonenumber");
					customer.setCustomerPhonenbr(sc.next());
					System.out.println("Enter your password");
					customer.setPassword(sc.next());
					
					if(customerService.registerCustomer(customer)) {
						System.out.println("You have been added");
					}else {
						System.out.println("You are not added");
					}
					
					break;
				}
				
				case 2:{
					System.out.println("Please provide login information\nPhone number:");
					String phoneNumber=sc.next();
					System.out.println("Password:");
					Customer customer=customerService.customerLogin(phoneNumber,sc.next());
					if(customer!=null) {
						System.out.println("Hello"+customer.getCustomerName());
						//Menu
						List<OrderItem> orderItems=new ArrayList<>();
						boolean loooper2=true;
						List<Category> categories= categoryService.getAllCategory();
						while(loooper2) {
							System.out.println("Showing category Menu");
							
							for(Category category:categories) {
								System.out.println(category.getCategoryId()+" "+category.getCategoryName());
							}
							System.out.println("Pleae choose:");
							//orate hobe
							
							List<Food> foods;
							try {
								foods = foodService.getFoodByCategoryId(sc.nextInt());
							
							if(foods.isEmpty())System.out.println("empty food");
							for(Food food:foods)System.out.println(food.getFoodId()+" "+food.getFoodName()+" "+food.getDescription()+" "+food.getPrice());
							System.out.println("To order food please provide the menu id");
							int foodId=sc.nextInt();
							Food food=foodService.getFoodById(foodId);
							OrderItem orderItem=new OrderItem();
							orderItem.setFoodId(foodId);
							System.out.println("Please give quantity:");
							int quantity=sc.nextInt();
							
							orderItem.setQuantity(quantity);
							orderItem.setSubtotal(food.getPrice()*quantity);
							orderItems.add(orderItem);
							
							System.out.println("To add more enter 1 else enter 2");
							if(sc.nextInt()==2) loooper2=false;
							
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								loooper2=false;
							}
							
							
							
						} 
						
						System.out.println("Please provide billing address:");
						orderService.createOrder(orderItems, customer.getCustomerId(), sc.next());
						
					}else {
						System.out.println("Invalid PhoneNumber or password");
					}
					
					break;
						
					}
					
					
				
				
				case 3:{
					
					looper=false;
					System.out.println("*Good Bye*");
					
					break;
				}
				default:{
					System.out.println("in valid entry!");
					break;
				}
			}
			
		}
		
		sc.close();
	
	}

}
