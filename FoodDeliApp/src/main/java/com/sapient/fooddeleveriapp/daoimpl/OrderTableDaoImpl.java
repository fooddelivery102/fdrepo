package com.sapient.fooddeleveriapp.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sapient.fooddeleveriapp.dao.OrderTableDAO;
import com.sapient.fooddeleveriapp.dbconfig.DBConnection;
import com.sapient.fooddeleveriapp.exception.OrderTableNotFoundException;
import com.sapient.fooddeleveriapp.model.OrderTable;

public class OrderTableDaoImpl implements OrderTableDAO {

	Connection connection;
	/*=====*/
	@Override
	
	public OrderTable addOrderTable(OrderTable orderTable) {
		connection = DBConnection.openConnection();
	String sql = "insert into Order_table(orderId,foodId,billingAddress,orderStatus,customerId,subcriptionId,orderDate,total) values(mysequence.nextval,?,?,?,?,?,?,?)";
	PreparedStatement statement = null;
   
	try {
		statement = connection.prepareStatement(sql,new String[] {"orderId"});
		//statement.setInt(1, orderTable.getFoodId());
		statement.setInt(1, orderTable.getOrderId());
		statement.setString(2, orderTable.getBillingAddress());
		statement.setString(3, orderTable.getOrderStatus()); 
		statement.setInt(4, orderTable.getCustomerId());
		statement.setInt(5, orderTable.getSubcriptionId());
		statement.setDate(6, orderTable.getOrderDate());
		statement.setDouble(7, orderTable.getTotal());
		statement.execute(); 
		
		ResultSet rs=statement.getGeneratedKeys();
		while(rs.next())
			orderTable.setOrderId(rs.getInt(1));
		rs.close();
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		try {
			if (statement != null)
				statement.close();
			DBConnection.closeConnection();
		} catch (SQLException e1) {
			e1.printStackTrace();
			
		}

	}
	return orderTable;
	

}

	@Override
	public boolean deleteOrderTableById(int orderid) {
		connection = DBConnection.openConnection();
		String sql= "delete from Order_table where orderid=?";
		boolean result=true;
		PreparedStatement ps=null;
		try {
			ps=connection.prepareStatement(sql);
			ps.setInt(1, orderid);
			result=ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if (ps != null)
					ps.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
				
			}
		}
		return result;
	}

	@Override
	public boolean updateOrderTableById(double total,int orderid) {
		// TODO Auto-generated method stub

		connection= DBConnection.openConnection();
		String sql= "UPDATE Order_table SET total =? WHERE orderid = ?";
		boolean result= true;
		PreparedStatement ps= null;
		try {
			ps= connection.prepareStatement(sql);
			ps.setDouble(1, total);
			ps.setInt(2, orderid);
			
			result= ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} 
			
			catch (SQLException e1) {
				e1.printStackTrace();
			} 
		
		}
		
		return result;
	}
	

	@Override
	public List<OrderTable> getAllOrderTables() {
	
	connection= DBConnection.openConnection();
	String sql= "select * from Order_Table";
	//boolean result= false;
	PreparedStatement ps= null;
	
	List<OrderTable> orderTableList= new ArrayList<>();
	try {
		ps= connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		
		ResultSet rs= ps.executeQuery();
		
		rs.beforeFirst();
		while(rs.next()) {
			OrderTable orderTable = new OrderTable();
			orderTable.setBillingAddress(rs.getString("BillingAddress"));
			orderTable.setCustomerId(rs.getInt("CustomerId"));
			orderTable.setFoodId(rs.getInt("FoodId"));
			orderTable.setOrderDate(rs.getDate("OrderDate"));
			orderTable.setOrderId(rs.getInt("OrderId"));
			orderTable.setOrderStatus(rs.getString("OrderStatus"));
			orderTable.setSubcriptionId(rs.getInt("SubcriptionId"));
			orderTable.setTotal(rs.getDouble("total"));
			orderTableList.add(orderTable);
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}finally {
		try {
			if(ps!=null)
				ps.close();
			DBConnection.closeConnection();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	
	}
	return orderTableList;
	}

	@Override
	public List<OrderTable> getOrderTableByOrderId(int orderId)throws OrderTableNotFoundException {
		connection= DBConnection.openConnection();
		String sql= "select * from Order_table where orderId IN (?)";
		//boolean result= false;
		PreparedStatement ps= null;
		
		List<OrderTable> OrderTableList= new ArrayList<OrderTable>();
		try {
			ps= connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setInt(1, orderId);
			ResultSet rs= ps.executeQuery();
			if(!rs.next()) {
				//throw new OrderIdNotFoundException("Author not Found");
			}
			rs.beforeFirst();
			while(rs.next()) {
				OrderTable orderTable= new OrderTable();
				orderTable.setOrderId(rs.getInt("OrderId"));
				orderTable.setBillingAddress(rs.getString("BillingAddress"));
				orderTable.setCustomerId(rs.getInt("CustomerId"));
				orderTable.setFoodId(rs.getInt("FoodId"));
				orderTable.setOrderDate(rs.getDate("OrderDate"));
				orderTable.setOrderStatus(rs.getString("OrderStatus"));
				orderTable.setSubcriptionId(rs.getInt("SubcriptionId"));
				orderTable.setTotal(rs.getDouble("total"));
				OrderTableList.add(orderTable);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new OrderTableNotFoundException("order not found");
		}finally {
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} 
			
			catch (SQLException e1) {
				e1.printStackTrace();
			} 
		
		}
		return OrderTableList;
	}

	@Override
	public List<OrderTable> getAllOrderTablesBySubscriptionId(int subscriptionId) throws OrderTableNotFoundException{
		connection= DBConnection.openConnection();
		String sql= "select * from Order_table where subcriptionid IN (?)";
		//boolean result= false;
		PreparedStatement ps= null;
		
		List<OrderTable> OrderTableList= new ArrayList<OrderTable>();
		try {
			ps= connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setInt(1, subscriptionId);
			ResultSet rs= ps.executeQuery();
			if(!rs.next()) {
				//throw new NotFoundException(" not Found");
			}
			rs.beforeFirst();
			while(rs.next()) {
				OrderTable orderTable= new OrderTable();
				orderTable.setOrderId(rs.getInt("OrderId"));
				orderTable.setBillingAddress(rs.getString("BillingAddress"));
				orderTable.setCustomerId(rs.getInt("CustomerId"));
				orderTable.setFoodId(rs.getInt("FoodId"));
				orderTable.setOrderDate(rs.getDate("OrderDate"));
				orderTable.setOrderStatus(rs.getString("OrderStatus"));
				orderTable.setSubcriptionId(rs.getInt("SubcriptionId"));
				orderTable.setTotal(rs.getDouble("total"));
				OrderTableList.add(orderTable);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new OrderTableNotFoundException("order not found");
		}finally {
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} 
			
			catch (SQLException e1) {
				e1.printStackTrace();
			} 
		
		}
		return OrderTableList;
	}

	@Override
	public OrderTable getOrderByCustomerId(int customerId) throws OrderTableNotFoundException{
		connection= DBConnection.openConnection();
		String sql= "select * from Order_table where customerId IN (?)";
		//boolean result= false;
		PreparedStatement ps= null;
		OrderTable orderTable= new OrderTable();
		
		try {
			ps= connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setInt(1, customerId);
			ResultSet rs= ps.executeQuery();
			if(!rs.next()) {
				//throw new NotFoundException("not Found");
			}
			rs.beforeFirst();
			while(rs.next()) {
				
				orderTable.setOrderId(rs.getInt("OrderId"));
				orderTable.setBillingAddress(rs.getString("BillingAddress"));
				orderTable.setCustomerId(rs.getInt("CustomerId"));
				orderTable.setFoodId(rs.getInt("FoodId"));
				orderTable.setOrderDate(rs.getDate("OrderDate"));
				orderTable.setOrderStatus(rs.getString("OrderStatus"));
				orderTable.setSubcriptionId(rs.getInt("SubcriptionId"));
				orderTable.setTotal(rs.getDouble("total"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new OrderTableNotFoundException("order not found");
		}finally {
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} 
			
			catch (SQLException e1) {
				e1.printStackTrace();
			} 
		
		}
		return orderTable;
	}
	@Override
	public List<OrderTable> getAllOrderTablesByStatus(String orderStatus)
			throws OrderTableNotFoundException {
		connection= DBConnection.openConnection();
		String sql= "select * from Order_table WHERE orderStatus = ? ";

		PreparedStatement ps= null;
		List<OrderTable> orderTableList= new ArrayList<>();
		try {
				ps= connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				
				ps.setString(1, orderStatus);
				ResultSet rs= ps.executeQuery();
				
				rs.beforeFirst();
				while(rs.next()) {
					OrderTable orderTable = new OrderTable();
					orderTable.setBillingAddress(rs.getString("BillingAddress"));
					orderTable.setCustomerId(rs.getInt("CustomerId"));
					orderTable.setFoodId(rs.getInt("FoodId"));
					orderTable.setOrderDate(rs.getDate("OrderDate"));
					orderTable.setOrderId(rs.getInt("OrderId"));
					orderTable.setOrderStatus(rs.getString("OrderStatus"));
					orderTable.setSubcriptionId(rs.getInt("SubcriptionId"));
					orderTable.setTotal(rs.getDouble("total"));
					orderTableList.add(orderTable);
				}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new OrderTableNotFoundException("order table not found");
		}finally {
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} 
			
			catch (SQLException e1) {
				e1.printStackTrace();
			} 
		
		}
		
		return orderTableList;
	}

	
	
}
