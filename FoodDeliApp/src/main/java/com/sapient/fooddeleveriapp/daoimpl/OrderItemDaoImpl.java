package com.sapient.fooddeleveriapp.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sapient.fooddeleveriapp.dao.OrderItemDAO;
import com.sapient.fooddeleveriapp.dbconfig.DBConnection;
import com.sapient.fooddeleveriapp.exception.OrderItemNotFoundException;
import com.sapient.fooddeleveriapp.model.Food;
import com.sapient.fooddeleveriapp.model.OrderItem;
import com.sapient.fooddeleveriapp.model.OrderTable;

public class OrderItemDaoImpl implements OrderItemDAO {

	Connection connection;

	public OrderItem addOrderItem(OrderItem orderItem) {

		connection = DBConnection.openConnection();
		String sql = "insert into OrderItem(foodId,orderId,orderitemId,quantity,subtotal) values(?,?,mysequence.nextval,?,?)";
		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(sql,new String[] {"orderitemId"});
			statement.setInt(1, orderItem.getFoodId());
			statement.setInt(2, orderItem.getOrderId());
			statement.setInt(3, orderItem.getQuantity());
			statement.setInt(4, orderItem.getSubtotal()); 
			//statement.setInt(5, orderItem.getOrderitemId());
			statement.execute(); 
			ResultSet rs=statement.getGeneratedKeys();
			while(rs.next())
				orderItem.setOrderitemId(rs.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
				
			}

		}
		return orderItem;
		

	}

	public boolean deleteOrderItemById(int orderitemId) {
		connection = DBConnection.openConnection();
		String sql= "delete from orderitem where orderitemid=?";
		boolean result=true;
		PreparedStatement ps=null;
		try {
			ps=connection.prepareStatement(sql);
			ps.setInt(1, orderitemId);
			result=ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if (ps != null)
					ps.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
				
			}
		}
		return result;
	}

	

	public List<OrderItem> getAllOrderItems() {
		
		connection= DBConnection.openConnection();
		String sql= "select * from orderItem";
		//boolean result= false;
		PreparedStatement ps= null;
		
		List<OrderItem> orderItemList= new ArrayList<>();
		try {
			ps= connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			
			ResultSet rs= ps.executeQuery();
			
			rs.beforeFirst();
			while(rs.next()) {
				OrderItem orderItem = new OrderItem();
				orderItem.setFoodId(rs.getInt("FoodId"));
				orderItem.setOrderId(rs.getInt("OrderId"));
				orderItem.setOrderitemId(rs.getInt("OrderitemId"));
				orderItem.setQuantity(rs.getInt("Quantity"));
				orderItem.setSubtotal(rs.getInt("Subtotal"));
				orderItemList.add(orderItem);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		
		}
		return orderItemList;
		}

	public List<OrderItem> getOrderItemByOrderId(int orderId) //throws OrderItemNotFoundException 
	{
		connection= DBConnection.openConnection();
		String sql= "select * from OrderItem where orderId IN (?)";
		PreparedStatement ps= null;
		
		List<OrderItem> orderItemList= new ArrayList<OrderItem>();
		try {
			ps= connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setInt(1, orderId);
			ResultSet rs= ps.executeQuery();
			if(!rs.next()) {
				//throw new OrderIdNotFoundException(" not Found");
			}
			rs.beforeFirst();
			while(rs.next()) {
				OrderItem orderItem= new OrderItem();
				orderItem.setOrderId(rs.getInt("OrderId"));
				orderItem.setFoodId(rs.getInt("FoodId"));
				orderItem.setOrderitemId(rs.getInt("OrderitemId"));
				orderItem.setQuantity(rs.getInt("Quantity"));
				orderItem.setSubtotal(rs.getInt("Subtotal"));
				orderItemList.add(orderItem);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//throw new OrderItemNotFoundException("Order item not found");
		}finally {
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} 
			
			catch (SQLException e1) {
				e1.printStackTrace();
			} 
		
		}
		return orderItemList;
	}

	@Override
	public boolean updateOrderItemById(int quantity,int orderitemId) {
		connection= DBConnection.openConnection();
		String sql= "UPDATE orderItem SET quantity = ? WHERE orderitemid = ?";
		boolean result= true;
		PreparedStatement ps= null;
		try {
			ps= connection.prepareStatement(sql);
			ps.setInt(1, quantity);
			ps.setInt(2, orderitemId);
			result= ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} 
			
			catch (SQLException e1) {
				e1.printStackTrace();
			} 
		
		}
		
		return result;
	}

	
	//edited by subha
	@Override
	public OrderItem getOrderItemById(int orderitemId) //throws OrderItemNotFoundException
	{
		connection = DBConnection.openConnection();
		String sql = "select * from orderItem where  orderitemid=?";
		PreparedStatement statement = null;
		OrderItem orderItem=new OrderItem();
		try {
			statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			statement.setInt(1, orderitemId);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				orderItem.setOrderId(rs.getInt("OrderId"));
				orderItem.setFoodId(rs.getInt("FoodId"));
				orderItem.setOrderitemId(rs.getInt("OrderitemId"));
				orderItem.setQuantity(rs.getInt("Quantity"));
				orderItem.setSubtotal(rs.getInt("Subtotal"));
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			//throw new OrderItemNotFoundException("Order item not found");

		}
		return orderItem;
	}

}
