package com.sapient.fooddeleveriapp.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sapient.fooddeleveriapp.dao.SubscriptionTypeDAO;
import com.sapient.fooddeleveriapp.dbconfig.DBConnection;
import com.sapient.fooddeleveriapp.exception.SubscriptionNotFoundException;
import com.sapient.fooddeleveriapp.exception.SubscriptionTypeNotFoundException;
import com.sapient.fooddeleveriapp.model.Subscription;
import com.sapient.fooddeleveriapp.model.SubscriptionType;

/**
 * @author Subhadeep Das
 *
 */
public class SubscriptionTypeDaoImpl implements SubscriptionTypeDAO{
	
	Connection connection=null;

	public List<SubscriptionType> getAllSubscriptionTypes() {
		// TODO Auto-generated method stub
		connection= DBConnection.openConnection();
		String sql= "select * from Subscription_type";
		List<SubscriptionType> types=new ArrayList<SubscriptionType>();
		PreparedStatement ps=null;
		
		try {
			 ps=connection.prepareStatement(sql);
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				SubscriptionType type=new SubscriptionType();
				type.setSubscriptionType(rs.getString("subscription_type"));
				type.setSubscriptionTypeId(rs.getInt("Subscription_typeid"));
				types.add(type);
		} 
			}catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}finally {
			
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
		}
		
		return types;
	}

	public SubscriptionType getSubscriptionTypeById(int subscriptionTypeId) throws SubscriptionTypeNotFoundException {
		// TODO Auto-generated method stub
		connection= DBConnection.openConnection();
		String sql= "select * from Subscription_type where Subscription_typeid=?";
		//boolean result= false;
		PreparedStatement ps= null;
		SubscriptionType type=new SubscriptionType();
		try {
			ps= connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setInt(1, subscriptionTypeId);
			ResultSet rs= ps.executeQuery();
			if(!rs.next()) {
				throw new SubscriptionTypeNotFoundException("id not found");
			}
			rs.beforeFirst();
			while(rs.next()) {
				
				type.setSubscriptionType(rs.getString("subscription_type"));
				type.setSubscriptionTypeId(rs.getInt("Subscription_typeid"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new SubscriptionTypeNotFoundException("id not found");
		}
		
		finally {
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		
		}
		return type;
	}

}
