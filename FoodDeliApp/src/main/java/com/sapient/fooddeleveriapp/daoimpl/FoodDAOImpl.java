package com.sapient.fooddeleveriapp.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sapient.fooddeleveriapp.dao.FoodDAO;
import com.sapient.fooddeleveriapp.dbconfig.DBConnection;
import com.sapient.fooddeleveriapp.exception.FoodNotFoundException;
import com.sapient.fooddeleveriapp.model.Food;

public class FoodDAOImpl implements FoodDAO {

	Connection connection;

	public void addFood(Food food) {

		connection = DBConnection.openConnection();

		String sql = "insert into food(foodid,foodName,categoryId,price,description) values(?,?,?,?,?)";
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, food.getFoodId());
			statement.setString(2, food.getFoodName());
			statement.setInt(3, food.getCategoryId());
			statement.setInt(4, food.getPrice());
			statement.setString(5, food.getDescription());
			statement.executeQuery();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			try {
				if (statement != null)
					statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e2) {
				System.out.println(e2.getMessage());
			}
		}

	}

	public boolean deleteFood(int foodid) {
		connection = DBConnection.openConnection();
		String sql = "delete from Food where foodid=?";
		PreparedStatement statement = null;
		boolean result = true;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, foodid);
			result = statement.execute();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e2) {
				System.out.println(e2.getMessage());
			}
		}
		return false;

	}

	public Food getFoodByCategoryId(int Categoryid) {
		connection = DBConnection.openConnection();
		String sql = "select * from Food where categoryid=?";
		PreparedStatement statement = null;
		Food food = new Food();
		try {
			statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			statement.setInt(1, Categoryid);
			ResultSet rs = statement.executeQuery();

			rs.beforeFirst();
			while (rs.next()) {
				food.setFoodName(rs.getString("FoodName"));
				food.setDescription(rs.getString("description"));
				food.setPrice(rs.getInt("price"));
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		//	throw new FoodNotFoundException("Food not found");
		}
		return food;

	}

	public boolean updateFood(int foodid, int price)

	{

		connection = DBConnection.openConnection();
		String sql = "update Food set price=? where foodid=?";
		PreparedStatement statement = null;
		boolean result = true;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, price);
			statement.setInt(2, foodid);
			result = statement.execute();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e2) {
				System.out.println(e2.getMessage());
			}
		}
		return result;

	}

	public Food getFoodByid(int foodId) //throws FoodNotFoundException

	{
		connection = DBConnection.openConnection();
		String sql = "select * from Food where foodid=?";
		PreparedStatement statement = null;
		Food food = new Food();
		try {
			statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			statement.setInt(1, foodId);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				food.setFoodName(rs.getString("FoodName"));
				food.setDescription(rs.getString("description"));
				food.setPrice(rs.getInt("price"));
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			//throw new FoodNotFoundException("food not found");
		}
		return food;

	}

	@Override
	public List<Food> getAllFoods() {
		connection = DBConnection.openConnection();
		String sql = "select * from Food";
		PreparedStatement statement = null;
		List<Food> foodList = new ArrayList<Food>();
		try {
			statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				Food food = new Food();
				food.setFoodName(rs.getString("FoodName"));
				food.setDescription(rs.getString("DESCRIPTION"));
				food.setFoodId(rs.getInt("FoodId"));
				food.setCategoryId(rs.getInt("CategoryId"));
				food.setPrice(rs.getInt("price"));

				foodList.add(food);
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return foodList;

	}

	@Override
	public List<Food> getAllFoodsByCategoryId(int categoryId) {
		List<Food> foodList=new ArrayList<>();
		connection = DBConnection.openConnection();
		String sql = "select * from Food where categoryId=?";
		PreparedStatement statement = null;
		
		try {
			statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			statement.setInt(1, categoryId);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				Food food = new Food();
				food.setFoodId(rs.getInt("foodid"));
				food.setFoodName(rs.getString("FoodName"));
				food.setDescription(rs.getString("description"));
				food.setPrice(rs.getInt("price"));
				foodList.add(food);
			}
			
			rs.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			

		}finally {
			try {
				if (statement != null)
					statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
				
			}

		}
		return foodList;

	}



	
	
	/*
	 * @Override public List<Food> getFoodByCategoryId(String category) throws
	 * FoodNotFoundException {
	 * 
	 * 
	 * connection=DBConnection.openConnection(); String
	 * sql="select * from Food where category like?"; PreparedStatement
	 * statement=null; List<Food> FoodList=new ArrayList<Food>(); try {
	 * statement=connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,
	 * ResultSet.CONCUR_READ_ONLY); statement.setString(1, "%"+category+"%");
	 * ResultSet rs=statement.executeQuery(); if(!rs.next()) throw new
	 * FoodNotFoundException("Food Not Found"); rs.beforeFirst(); while (rs.next())
	 * { Food food =new Food(); food.setFoodName(rs.getString("FoodName"));
	 * food.setDescription(rs.getString("DESCRIPTION"));
	 * food.setFoodId(rs.getInt("FoodId"));
	 * food.setCategoryId(rs.getInt("CategoryId"));
	 * food.setPrice(rs.getInt("price")); FoodList.add(food); }
	 * 
	 * } catch (SQLException e) { System.out.println(e.getMessage());
	 * 
	 * return FoodList; } return FoodList;
	 * 
	 * }
	 */
}
