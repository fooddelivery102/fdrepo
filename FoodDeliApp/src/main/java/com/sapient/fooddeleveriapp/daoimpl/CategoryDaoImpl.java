package com.sapient.fooddeleveriapp.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sapient.fooddeleveriapp.dao.CategoryDAO;
import com.sapient.fooddeleveriapp.dbconfig.DBConnection;
import com.sapient.fooddeleveriapp.exception.CategoryNotFoundException;

import com.sapient.fooddeleveriapp.model.Category;
import com.sapient.fooddeleveriapp.model.Customer;
import com.sapient.fooddeleveriapp.model.Food;

public class CategoryDaoImpl implements CategoryDAO {


	Connection connection;

	public void addCategory(Category category) {
		connection = DBConnection.openConnection();
		String sql = "insert into CATEGORY(categoryName,categoryId,) values(?,mysequence.nextval)";
		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(sql);
			statement.setString(1, category.getCategoryName());		
			statement.execute(); 
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
				
			}
		}

		}
	public boolean deleteCategory(int categoryId) {
		connection = DBConnection.openConnection();
		String sql= "delete from category where categoryId=?";
		boolean result=true;
		PreparedStatement ps=null;
		try {
			ps=connection.prepareStatement(sql);
			ps.setInt(1, categoryId);
			result=ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if (ps != null)
					ps.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
				
			}
		}
		return result;
	}

//**
	public Category getCategoryById(int categoryId) throws CategoryNotFoundException{

		connection=DBConnection.openConnection();
		String sql="select * from Category where categoryid=?";
		PreparedStatement statement=null;
		Category category=new Category();
		try {
			statement=connection.prepareStatement(sql,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			statement.setInt(1,  categoryId);
			ResultSet rs=statement.executeQuery();
			if(!rs.next())
				throw new  CategoryNotFoundException();
			rs.beforeFirst();
			while (rs.next()) {
				category.setCategoryName(rs.getString("categoryname"));
				category.setCategoryId(rs.getInt("categoryid"));
				
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new CategoryNotFoundException();
		}
		return category;
	}
	public boolean updateCategory(int categoryId) {
		
		connection=DBConnection.openConnection();
		String sql="update Category set categoryname=? categoryid=?";
		PreparedStatement statement=null;
		boolean result=true;
		try {
			statement=connection.prepareStatement(sql);
			statement.setInt(1, categoryId);
		
		    result=statement.execute();
		} catch (SQLException e) {
			System.out.println(e.getMessage());}
		finally {
			try {
				if(statement!=null)statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e2) {
				System.out.println(e2.getMessage());
			}
					}
		return false;
}
	@Override
	public List<Category> getAllCategories() {
		
		
		connection=DBConnection.openConnection();
		String sql="select * from Category";
		PreparedStatement statement=null;
	     List<Category> categoryList=new ArrayList<Category>();
		try {
			statement=connection.prepareStatement(sql,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs=statement.executeQuery();
			while (rs.next()) {
				Category category =new Category();
				category.setCategoryName(rs.getString("CategoryName"));
				category.setCategoryId(rs.getInt("CategoryId"));
				categoryList.add(category);
			
			}	
			
			rs.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				if (statement != null)
					statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
				
			}

		}
		return categoryList;
	

	}
	
}

	

