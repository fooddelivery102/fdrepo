package com.sapient.fooddeleveriapp.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.sapient.fooddeleveriapp.dao.SubscriptionDAO;
import com.sapient.fooddeleveriapp.dbconfig.DBConnection;
import com.sapient.fooddeleveriapp.exception.SubscriptionNotFoundException;
import com.sapient.fooddeleveriapp.model.Subscription;

/**
 * @author Subhadeep Das
 *
 */
public class SubscriptionDaoImpl implements SubscriptionDAO {
	
	Connection connection;

	

	
	
	public Subscription insertSubscription(Subscription subscription) {
		// TODO Auto-generated method stub
		String sql= "insert into Subscription_table(subcriptionid, subscription_date, subscription_time,customerid, Subscription_typeid)"
				+" values( mysequence.nextval,?,?,?,?)";
		PreparedStatement statement= null;
		try {
			connection=DBConnection.openConnection();
			statement=connection.prepareStatement(sql);
			statement.setDate(1, subscription.getSubscriptionDate());
			statement.setInt(2, subscription.getSubscriptionTime());
			statement.setInt(3, subscription.getCustomerId());
			statement.setInt(4, subscription.getSubscriptionTypeId());
			statement.execute();
			ResultSet rs=statement.getGeneratedKeys();
			while(rs.next())
			subscription.setSubscriptionId(rs.getInt(1));
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}finally {
			try{
				if(statement!=null)
					statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
				return null;
			}
			
		}
		
		return subscription;
	}
	
	
	

	
	public List<Subscription> getAllsubscriptions() {
		// TODO Auto-generated method stub
		connection= DBConnection.openConnection();
		String sql= "select * from Subscription_table";
		List<Subscription> subscriptions=new ArrayList<Subscription>();
		PreparedStatement ps=null;
		
		try {
			 ps=connection.prepareStatement(sql);
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				Subscription subscription=new Subscription();
				subscription.setCustomerId(rs.getInt("subcriptionid"));
				subscription.setSubscriptionDate(rs.getDate("subscription_date"));
				subscription.setSubscriptionId(rs.getInt("subcriptionid"));
				subscription.setSubscriptionTime(rs.getInt("subscription_time"));
				subscription.setSubscriptionTypeId(rs.getInt("Subscription_typeid"));
				subscriptions.add(subscription);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}finally {
			
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
		}
		
		return subscriptions;
		
	}

	public Subscription getSubscriptionById(int subscriptionId) throws SubscriptionNotFoundException {
		// TODO Auto-generated method stub
		connection= DBConnection.openConnection();
		String sql= "select * from Subscription_table where subcriptionid=?";
		//boolean result= false;
		PreparedStatement ps= null;
		Subscription subscription=new Subscription();
		try {
			ps= connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setInt(1, subscriptionId);
			ResultSet rs= ps.executeQuery();
			if(!rs.next()) {
				throw new SubscriptionNotFoundException("ID not Found");
			}
			rs.beforeFirst();
			while(rs.next()) {
				
				subscription.setCustomerId(rs.getInt("subcriptionid"));
				subscription.setSubscriptionDate(rs.getDate("subscription_date"));
				subscription.setSubscriptionId(rs.getInt("subcriptionid"));
				subscription.setSubscriptionTime(rs.getInt("subscription_time"));
				subscription.setSubscriptionTypeId(rs.getInt("Subscription_typeid"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new SubscriptionNotFoundException("id not found");
		}
		
		finally {
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		
		}
		return subscription;
	}
	

	

	public boolean deleteSubscriptionById(int subscriptionId) throws SubscriptionNotFoundException {
		// TODO Auto-generated method stub
		connection= DBConnection.openConnection();
		String sql= "delete from Subscription_table where subcriptionid=?";
		boolean result= false;
		PreparedStatement ps= null;
		try {
			ps= connection.prepareStatement(sql);
			ps.setInt(1, subscriptionId);
			result= ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(ps!=null)
					ps.close();
				DBConnection.closeConnection();
			} 
			
			catch (SQLException e1) {
				e1.printStackTrace();
				throw new SubscriptionNotFoundException("id not there");
			} 
		
		}
		
		return result;
	}


	
	
	

}
