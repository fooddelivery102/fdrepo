package com.sapient.fooddeleveriapp.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sapient.fooddeleveriapp.dao.CustomerDAO;
import com.sapient.fooddeleveriapp.dbconfig.DBConnection;
import com.sapient.fooddeleveriapp.exception.CustomerNotFoundException;
import com.sapient.fooddeleveriapp.model.Customer;

public class CustomerDAOImpl implements CustomerDAO{

	private static final boolean Customer = false;
	Connection connection;
	
	public boolean addCustomer(Customer customer) {
		connection=DBConnection.openConnection();
		String sql="insert into Customer(Name,phonenumber,Address,password,CustomerId) values(?,?,?,?,mysequence.nextval)";
		PreparedStatement statement=null;
		try {
			statement=connection.prepareStatement(sql);
			statement.setString(1, customer.getCustomerName());
			statement.setString(2,customer.getCustomerPhonenbr());
			statement.setString(3, customer.getCustomerAddress());
			statement.setString(4,customer.getPassword());
			statement.execute();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;}
		finally {
			try {
				if(statement!=null)statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e2) {
				System.out.println(e2.getMessage());
			}
		}
		
		return true;
		}

	public boolean deleteCustomer(int CustomerId) {
		connection=DBConnection.openConnection();
		String sql="delete from Customer where CustomerId=?";
		PreparedStatement statement=null;
		boolean result=true;
		try {
			statement=connection.prepareStatement(sql);
			statement.setInt(1, CustomerId);
		    result=statement.execute();
		} catch (SQLException e) {
			System.out.println(e.getMessage());}
		finally {
			try {
				if(statement!=null)statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e2) {
				System.out.println(e2.getMessage());
			}
					}
		return false;
	}
	public Customer getCustomerById(int CustomerId)
	{
		connection=DBConnection.openConnection();
		String sql="select * from Customer where CustomerId=?";
		PreparedStatement statement=null;
		 Customer  customer =new  Customer();
		try {
			statement=connection.prepareStatement(sql,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			statement.setInt(1,  CustomerId);
			ResultSet rs=statement.executeQuery();
			if(!rs.next())
				throw new  CustomerNotFoundException();
			rs.beforeFirst();
			while (rs.next()) {
				 customer.setCustomerName(rs.getString("name"));
				 customer.setCustomerPhonenbr(rs.getString("phonenumber"));
				 customer.setCustomerAddress(rs.getString("Address"));
				 customer.setPassword(rs.getString("password"));
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}catch ( CustomerNotFoundException e) {
			System.out.println(e.getMessage());
			try {
				throw e;
			} catch (CustomerNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return customer;
	}

	public boolean updateCustomer(int CustomerId) {
		
		connection=DBConnection.openConnection();
		String sql="update Customer set customername=? where customerid=?";
		PreparedStatement statement=null;
		boolean result=true;
		try {
			statement=connection.prepareStatement(sql);
			statement.setInt(1, CustomerId);
		
		    result=statement.execute();
		} catch (SQLException e) {
			System.out.println(e.getMessage());}
		finally {
			try {
				if(statement!=null)statement.close();
				DBConnection.closeConnection();
			} catch (SQLException e2) {
				System.out.println(e2.getMessage());
			}
					}
		return false;

}

	
	

	public Customer authenticateCustomer(String CustomerPhonenbr, String Customerpassword) 
	
	{
		connection=DBConnection.openConnection();
	/*.........*/
		String sql="select * from Customer where phonenumber=? and password=?";
		PreparedStatement statement=null;
		 Customer  customer =null;
		 try {
			 customer=new Customer();
				statement=connection.prepareStatement(sql,
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_READ_ONLY);
				statement.setString(1,  CustomerPhonenbr);
				statement.setString(2,  Customerpassword);
				ResultSet rs=statement.executeQuery();
				if(!rs.next())
					throw new  CustomerNotFoundException();
				rs.beforeFirst();
				while (rs.next()) 
				{
					 customer.setCustomerId(rs.getInt("CustomerId"));
					 customer.setCustomerName(rs.getString("name"));
					 customer.setCustomerPhonenbr(rs.getString("phonenumber"));
					 customer.setPassword(rs.getString("password"));
					 customer.setCustomerAddress(rs.getString("Address"));
					
					 
				}
			} 
		 catch (SQLException e) {
					System.out.println(e.getMessage());
				}
		 catch ( CustomerNotFoundException e) {
					
						//throw e;
					
				}	
		return customer;
				}
	}



