package com.sapient.fooddeleveriapp.service;

import java.util.List;

import com.sapient.fooddeleveriapp.model.OrderItem;

public interface OrderItemService {

	public void addOrderItem(OrderItem orderItem);
	public boolean deleteOrderItemById(int orderitemId);
	public boolean updateOrderItemById(int orderitemId,int quantity);
	List<OrderItem> getAllOrderItems();
	List<OrderItem> getOrderItemByOrderId(int orderId);
}
