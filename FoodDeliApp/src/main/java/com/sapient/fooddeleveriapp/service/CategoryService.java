package com.sapient.fooddeleveriapp.service;

import java.util.List;

import com.sapient.fooddeleveriapp.model.Category;

public interface CategoryService {
	List<Category>getAllCategory();
	Category getCategoryById(int categoryId);
}
