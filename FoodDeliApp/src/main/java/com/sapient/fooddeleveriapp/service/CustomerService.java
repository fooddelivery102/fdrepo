package com.sapient.fooddeleveriapp.service;

import com.sapient.fooddeleveriapp.model.Customer;

public interface CustomerService {
	Customer customerLogin(String...info);
	boolean registerCustomer(Customer customer);
	
	

}
