package com.sapient.fooddeleveriapp.service;
import java.util.List;

import com.sapient.fooddeleveriapp.model.*;

public interface OrderService {

	boolean createOrder(List<OrderItem> orderItems, int customerId,String billingAddress);
	List<OrderTable> getAllNewOrders();
		
		
		
}
