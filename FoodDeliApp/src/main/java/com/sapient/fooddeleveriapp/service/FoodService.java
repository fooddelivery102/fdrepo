package com.sapient.fooddeleveriapp.service;

import java.util.List;

import com.sapient.fooddeleveriapp.exception.FoodNotFoundException;
import com.sapient.fooddeleveriapp.model.Food;

/**
 * @author subdas3
 *
 */
public interface FoodService {
	List<Food>getFoodByCategoryId(int categoryId) ;
	Food getFoodById(int id);
	Food getFoodByOrderItemId(int orderItemId);
}
