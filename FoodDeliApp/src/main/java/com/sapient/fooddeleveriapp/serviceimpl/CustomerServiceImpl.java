package com.sapient.fooddeleveriapp.serviceimpl;

import com.sapient.fooddeleveriapp.dao.CustomerDAO;
import com.sapient.fooddeleveriapp.daoimpl.CustomerDAOImpl;
import com.sapient.fooddeleveriapp.model.Customer;
import com.sapient.fooddeleveriapp.service.CustomerService;

/**
 * @author subdas3
 *
 */
public class CustomerServiceImpl implements CustomerService {
	private CustomerDAO dao;

	@Override
	public Customer customerLogin(String... info) {
		// TODO Auto-generated method stub
		dao=new  CustomerDAOImpl();
		return dao.authenticateCustomer(info[0], info[1]);
		
		
	}

	@Override
	public boolean registerCustomer(Customer customer) {
		// TODO Auto-generated method stub
		dao=new  CustomerDAOImpl();
		return dao.addCustomer(customer);
		
	}

}
