package com.sapient.fooddeleveriapp.serviceimpl;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.sapient.fooddeleveriapp.dao.OrderItemDAO;
import com.sapient.fooddeleveriapp.dao.OrderTableDAO;
import com.sapient.fooddeleveriapp.daoimpl.OrderItemDaoImpl;
import com.sapient.fooddeleveriapp.daoimpl.OrderTableDaoImpl;
import com.sapient.fooddeleveriapp.exception.OrderTableNotFoundException;
import com.sapient.fooddeleveriapp.model.OrderItem;
import com.sapient.fooddeleveriapp.model.OrderTable;

/**
 * @author subdas3
 *
 */
public class OrderServiceImpl implements com.sapient.fooddeleveriapp.service.OrderService {
	
	OrderTableDAO orderDao=new OrderTableDaoImpl();
	OrderItemDAO orderItemDao=new OrderItemDaoImpl();

	@Override
	public boolean createOrder(List<OrderItem> orderItems, int customerId,String billingAddress) {
		// TODO Auto-generated method stub
		OrderTable orderTable=new OrderTable();
		orderTable.setCustomerId(customerId);
		orderTable.setBillingAddress(billingAddress);
		orderTable.setOrderDate(Date.valueOf(LocalDate.now()));
		orderTable.setOrderStatus("NEW");
		orderTable.setSubcriptionId(0);
		orderTable.setFoodId(0);
		orderTable.setTotal(0.0);
		double total=0;
		 orderTable=orderDao.addOrderTable(orderTable);
		 System.out.println("adding order is working ? orderid"+orderTable.getOrderId());
		 for(OrderItem oderItem: orderItems) {
			 oderItem.setOrderId(orderTable.getOrderId());
			 total+=oderItem.getSubtotal();
			 orderItemDao.addOrderItem(oderItem);
			 
		 }
		orderTable.setTotal( total);
		
		orderDao.updateOrderTableById(total,orderTable.getOrderId());
		
		return false;
	}

	@Override
	public List<OrderTable> getAllNewOrders() {
		// TODO Auto-generated method stub
		List<OrderTable> list=new ArrayList<>();
		try {
			list = orderDao.getAllOrderTablesByStatus("NEW");
		} catch (OrderTableNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	//	if(list.isEmpty())throw new OrderTableNotFoundException("hash");
		return list;
		
	}

}
