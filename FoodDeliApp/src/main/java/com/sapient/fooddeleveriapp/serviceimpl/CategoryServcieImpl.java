package com.sapient.fooddeleveriapp.serviceimpl;
//sdfg

import java.util.List;

import com.sapient.fooddeleveriapp.dao.CategoryDAO;
import com.sapient.fooddeleveriapp.daoimpl.CategoryDaoImpl;
import com.sapient.fooddeleveriapp.exception.CategoryNotFoundException;
import com.sapient.fooddeleveriapp.model.Category;
import com.sapient.fooddeleveriapp.service.CategoryService;

public class CategoryServcieImpl implements CategoryService {
	
	CategoryDAO categoryDAO;

	@Override
	public List<Category> getAllCategory() {
		categoryDAO=new CategoryDaoImpl();
		return categoryDAO.getAllCategories();
	}

	@Override
	public Category getCategoryById(int categoryId) {
		categoryDAO=new CategoryDaoImpl();
		try {
			return categoryDAO.getCategoryById(categoryId);
		} catch (CategoryNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

}
