package com.sapient.fooddeleveriapp.serviceimpl;

import java.util.List;

import com.sapient.fooddeleveriapp.dao.FoodDAO;
import com.sapient.fooddeleveriapp.dao.OrderItemDAO;
import com.sapient.fooddeleveriapp.daoimpl.FoodDAOImpl;
import com.sapient.fooddeleveriapp.daoimpl.OrderItemDaoImpl;
import com.sapient.fooddeleveriapp.model.OrderItem;
import com.sapient.fooddeleveriapp.service.OrderItemService;

public class OrderItemServiceImpl implements OrderItemService {

	OrderItemDAO orderItemDAO=new OrderItemDaoImpl();
	FoodDAO foodDao=new FoodDAOImpl();
	
	
	@Override
	public void addOrderItem(OrderItem orderItem) {
		
		
		orderItemDAO.addOrderItem(orderItem);
	}

	@Override
	public boolean deleteOrderItemById(int orderitemId) {
		//boolean orderItem=orderItemDAO.deleteOrderItemById(orderitemId);
		//if 
		return orderItemDAO.deleteOrderItemById(orderitemId);
	}

	@Override
	public boolean updateOrderItemById(int orderitemId,int quantity) {
		return orderItemDAO.updateOrderItemById(orderitemId, quantity);
	}

	@Override
	public List<OrderItem> getAllOrderItems() {
		return orderItemDAO.getAllOrderItems();
	}

	@Override
	public List<OrderItem> getOrderItemByOrderId(int orderId) {
		return null;
	}
	
	

}


