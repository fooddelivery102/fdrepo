package com.sapient.fooddeleveriapp.serviceimpl;

import java.util.List;

import com.sapient.fooddeleveriapp.dao.FoodDAO;
import com.sapient.fooddeleveriapp.dao.OrderItemDAO;
import com.sapient.fooddeleveriapp.daoimpl.FoodDAOImpl;
import com.sapient.fooddeleveriapp.daoimpl.OrderItemDaoImpl;
import com.sapient.fooddeleveriapp.exception.FoodNotFoundException;
import com.sapient.fooddeleveriapp.exception.OrderItemNotFoundException;
import com.sapient.fooddeleveriapp.model.Food;
import com.sapient.fooddeleveriapp.model.OrderItem;
import com.sapient.fooddeleveriapp.service.FoodService;

public class FoodServiceImpl implements FoodService {
	
	
	FoodDAO foodDAO;
	OrderItemDAO orderItemDAO;
	
	@Override
	public List<Food> getFoodByCategoryId(int categoryId)  {
		foodDAO=new FoodDAOImpl();
		List<Food> foods=foodDAO.getAllFoodsByCategoryId(categoryId);
		//if(foods==null)throw new FoodNotFoundException("no food");
		return foods;
		
	}

	@Override
	public Food getFoodById(int id) {
		foodDAO=new FoodDAOImpl();
		Food food= foodDAO.getFoodByid(id);
		//if(food==null)throw new FoodNotFoundException("no food");
		return food;
	}

	@Override
	public Food getFoodByOrderItemId(int orderItemId)  {
		foodDAO=new FoodDAOImpl();
		orderItemDAO=new OrderItemDaoImpl();
		OrderItem item;
	
			item = orderItemDAO.getOrderItemById(orderItemId);
			return foodDAO.getFoodByid(item.getFoodId());
		
		
	}

}
